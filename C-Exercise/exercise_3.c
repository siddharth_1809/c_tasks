/*Exercise-3
Write a C program to reverse the given number in most efficient way.
*/


#include <stdio.h>

int main()
{
    int num,rem;
    int result=0,temp=1;

    printf("Enter a number: ");
    scanf("%d",&num);

    while(num>0)
    {
        rem = num%10;
        result = result*10 + rem;
        num /= 10;
    }
    printf("Reverse is %d",result);
}
