/*Exercise-4
Write a C function to invert the bits in given integer variable.
The inversion of bits should start from given position
and should be done upto given counts.
*/


#include<stdio.h>

void invert(int, int, int);
int binary(int);

int main()
{
    int num, result, position , count;
    printf("Enter a number: ");
    scanf("%d",&num);
    printf("enter a position: ");
    scanf("%d",&position);
    printf("Enter count: ");
    scanf("%d",&count);
    printf("\nInput is %d",binary(num));
    invert(num, position, count);
}


void invert(int num, int position, int count)
{
    int i, start;
    start = position;
    for(i=0; i<count;i++)
    {
        num = num ^ (1<<position);
        position++;
    }
    printf("\nAfter inverting from position %d upto count %d result is %d",start,count,binary(num));
}

int binary(int num)
{
    int rem, temp=1, bin=0;
    while(num>0)
    {
        rem = num%2;
        bin = bin + rem*temp;
        num /= 2;
        temp *= 10;
    }
    return bin;
}
