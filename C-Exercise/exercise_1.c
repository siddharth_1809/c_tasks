/*Exercise-1
Write a C program that display given number in string format.
e.g. 10 -> ten 100 -> hundred 5270 -> five thousand two hundred seventy
*/

#include<stdio.h>

int main()
{
    int temp,num,i=0,arr[4];
    char *ones[] = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
    char *elevens[] = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
    char *tens[] = {"", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
    printf("Enter 4 digit number: ");
    scanf("%d", &num);
    temp=num;

    while(temp>0)
    {
        arr[i]=temp%10;
        temp/=10;
        i++;
    }
    if(i==4)
        printf ("%s Thousand ", ones[arr[3]]);
    if(i>=3 && arr[2]!=0)
        printf(" %s Hundred ",ones[arr[2]]);
    if(i>=2)
    {
        if(arr[1]==0)
            printf("%s\n",ones[arr[0]]);
        else if(arr[1]==1)
            printf("%s\n",elevens[arr[0]]);
        else
            printf("%s %s\n",tens[arr[1]],ones[arr[0]]);
    }
    if(i==1 && num!=0)
        printf ("%s\n", ones[arr[0]]);
    if(num==0)
        printf("Zero\n");
}
