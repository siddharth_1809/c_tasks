/*Exercise-8
Find string length without using library function (strlen).
*/


#include<stdio.h>
#define SIZE 100
int main()
{
    char str[SIZE];
    int count=0, i=0;
    printf("Enter a String: ");
    gets(str);
    //use get() instead of scanf() because scanf() terminates on whitespace
    //null stops reading only when it encounters a newline and replaces newline by the null character.

    while(str[i]!='\0')
    {

        i++;
        count++;
    }
    printf("Length of %s is %d",str,count);
}
