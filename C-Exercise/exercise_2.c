/*Exercise-2
Write a C program to find 1's count in binary value of given integer number.
*/

#include<stdio.h>
int binary(int);
int main()
{
    int num,i,ones, temp;
    printf("Enter a Number: ");
    scanf("%d",&num);
    temp = num;
    ones = 0;

    for(i=0;i<=sizeof(num)*8;i++)
    {
        if(num & 1)
        {
            ones++;
        }
        num >>= 1;
    }
    printf("Binary of %d is %d and Number of ones are %d\n",temp,binary(temp),ones);
}

int binary(int num)
{
    int rem, temp=1, bin=0;
    while(num>0)
    {
        rem = num%2;
        bin = bin + rem*temp;
        num /= 2;
        temp *= 10;
    }
    return bin;
}
