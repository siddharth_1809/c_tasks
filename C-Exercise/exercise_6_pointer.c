/*Using concept of pointer */

#include<stdio.h>
#include<string.h>
#define SIZE 20

void expand_str(char *);
int main()
{
    char str[SIZE];
    char *ptr;
    ptr = str;
    printf("Enter input String: ");
    gets(str);

    while(*ptr != '\0')
    {
        if(*ptr != '-')
            ptr++;
        else
        {
            for(char *i=ptr; *i != '\0'; i++)
            {
                *i = *(i+1);
            }
            ptr++;
        }
    }

    printf("Output String is: ");
    printf("%s",str);
    expand_str(str);
}

void expand_str(char *ptr)
{
    int i;
    char alpha[SIZE], num[SIZE];
    char *a, *n;
    a = alpha;
    n = num;

    while(*ptr != '\0')
    {
        if((*ptr >='A' && *ptr<='Z') || (*ptr>='a' && *ptr<='z'))
        {
            *a = *ptr;
            a++;
            ptr++;
        }
        else
        {
            *n = *ptr;
            n++;
            ptr++;
        }
    }
    *a = *n = '\0';

    printf("\n");

    for(i=alpha[0]; i<=alpha[strlen(alpha)-1]; i++)
    {
        printf("%c",i);
    }
    for(i=num[0]; i<=num[strlen(num)-1]; i++)
    {
        printf("%c",i);
    }
}

