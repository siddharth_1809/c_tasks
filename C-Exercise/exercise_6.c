/*Exercise-6
Write a function that expands given short norm to expanded string.
e.g. if passed string is "a-g" output should be "abcdefg"
if passed string is "b-h4-8" output should be "bcdefgh45678"
if passed string is "-c-e-" output should be "cde"
*/


#include<stdio.h>
#include<string.h>
#define SIZE 20

void expand_str(char []);
int main()
{
    char str[SIZE];
    int i,j;
    printf("Enter input String: ");
    gets(str);

    for(i=0; str[i]!='\0'; i++)
    {
        while(str[i]=='-')
        {
            for(j=i; str[j]!='\0'; j++)
            {
                str[j]=str[j+1];
            }
            str[j] = '\0';
        }
    }
    printf("Output String is: ");
    puts(str);
    expand_str(str);
}

void expand_str(char str[])
{
    int i, a=0, n=0;                        //a -> alpha array index  n-> num array index
    char alpha[SIZE], num[SIZE];            //alpha -> to store alphabet    num-> to store numbers
    for(i=0; str[i]!='\0'; i++)
    {
        if((str[i]>='A' && str[i]<='Z') || (str[i]>='a' && str[i]<='z'))
        {
            alpha[a] = str[i];
            a++;
        }
        else
        {
            num[n] = str[i];
            n++;
        }
    }
    alpha[a] = num[n] = '\0';
    for(i=alpha[0]; i<=alpha[strlen(alpha)-1]; i++)
    {
        printf("%c",i);
    }
    for(i=num[0]; i<=num[strlen(num)-1]; i++)
    {
        printf("%c",i);
    }
}
