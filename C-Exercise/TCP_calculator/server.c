/* Server Side */


#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<netinet/in.h>  //contains constants and structure needed for internet domain address
#include<sys/types.h>   //contains number of data types used in system calls
#include<sys/socket.h>  //contains definition of structure needed for socket    //sockaddr

int main(int argc, char *argv[])    //argc for number of argument and argv[0]->filename argv[1]->port no.
{
    int sockfd, newsockfd, port;
    socklen_t clinlen;      //socket related parameter

    struct sockaddr_in serveradd, clientadd;

    if(argc < 2)
    {
        printf("Port is not provided\n");
        exit(1);
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if(sockfd < 0)
    {
        printf("Socket creation failed\n");
    }

//bzero(void *s, size_t n) erases data in the n bytes of memory starting at location pointed by S
    bzero((char *)&serveradd, sizeof(serveradd));

    port = atoi(argv[1]);   //converts string argument to an integer

    serveradd.sin_family = AF_INET;
    serveradd.sin_addr.s_addr = INADDR_ANY;
    serveradd.sin_port = htons(port);   //translate an unsigned short int to network byte order

    if(bind(sockfd, (struct sockaddr *)&serveradd, sizeof(serveradd)) < 0)
    {
        printf("Binding failed\n");
    }

    listen(sockfd, 5);
    clinlen = sizeof(clientadd);

    newsockfd = accept(sockfd, (struct sockaddr *)clientadd, &clinlen);

    if(newsockfd < 0)
    {
        printf("Accept fails\n");
    }

    int in1, in2, choice, result;

    entry : read(newsockfd, &in1, sizeof(int));
            printf("Number-1 is: %d\n",in1);
            read(newsockfd, &in2, sizeof(int));
            printf("Number-2 is: %d\n",in2);
            read(newsockfd, &choice, sizeof(int));
            printf("Choice is: %d\n",choice);

            switch(choice)
            {
                case 1 : result = in1 + in2; break;
                case 2 : result = in1 - in2; break;
                case 3 : result = in1 * in2; break;
                case 4 : result = in1 / in2; break;
                case 5 : goto stop; break;
                default : printf("Enter a valid choice\n"); break;
            }
            if(choice != 5)
            {
                goto entry;
            }
            write(newsockfd, &result, sizeof(int));

    stop : close(newsockfd);
    close(sockfd);
}
