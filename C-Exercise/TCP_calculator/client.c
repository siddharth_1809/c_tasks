/* Client Side */

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<netinet/in.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netdb.h>   //for struct hostnet

int main(int argc, char *argv[])    //total 3 arguments argv[0]->filename argv[1]->127.0.0.1 argv[2]->port no.
{
    int sockfd, port;
    struct sockaddr_in serveradd;
    struct hostnet *server;     //structure describes an internet host

    if(argc < 3)
    {
        printf("Insufficient argument\n");
    }

    port = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if(sockfd < 0)
    {
        printf("Socket creation failed\n");
    }

    server = gethostbyname(argv[1]);
    if(server == NULL)
    {
        printf("Server is unavailable\n");
    }

    bzero((char *)&serveradd, sizeof(serveradd));
    serveradd.sin_family = AF_INET;

    bcopy((char *)server->h_addr, (char *)&serveradd.sin_addr.s_addr, server->h_length);

    serveradd.sin_port = htons(port);

    if(connect(sockfd, (struct sockaddr *)&serveradd, sizeof(serveradd)) < 0)
    {
        printf("Error in Connection\n");
    }

    int in1, in2, reult, choice;

    entry : printf("Enter number-1: ");
            scanf("%d",&in1);
            write(sockfd, &in1, sizeof(int));

            printf("Enter number-2: ");
            scanf("%d",&in2);
            write(sockfd, &in2, sizeof(int));

            printf("Available operations : \n1.Addition :\n2.Subtraction :\n3.Multiplication :\n4.Division :\n5.Exit :\n");

            printf("Enter choice: ");
            scanf("%d",&choice);
            write(sockfd, &choice, sizeof(int));

            if(choice == 5)
            {
                goto stop;
            }

            read(sockfd, &result, sizeof(int));
            printf("\nThe answer is : %d",result);

            if(choice != 5)
            {
                goto entry;
            }

    stop : printf("Exit Operation\n");
    close(sockfd);
}
