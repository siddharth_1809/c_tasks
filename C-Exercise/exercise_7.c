/*Exercise-7
Make an example of fifo
*/


//Queue follows a rule of FIFO
//insert, delete, display operation
#include<stdio.h>
#define SIZE 5

int queue[SIZE];
int front=0;    //for deletion operation
int rear=0; //for insertion operation

void insert();
void deletion();
void traverse();

void main()
{
    int option,ele;
    while(1)
    {
        printf("\n1.Insert\n");
        printf("2.Delete\n");
        printf("3.Traverse\n");
        printf("4.Exit\n");

        printf("Select an option: ");
        scanf("%d",&option);

        switch(option)
        {
            case 1: insert();
                    break;
            case 2: deletion();
                    break;
            case 3: traverse();
                    break;
            case 4: exit(0);
            default : printf("Select a valid option\n"); break;
        }
    }
}

void insert()
{
    if(SIZE==rear)
    {
        printf("Queue is full\n");
    }
    else
    {
        int element;
        printf("Enter an element: ");
        scanf("%d",&element);
        queue[rear] = element;
        rear++;
    }
}
void deletion()
{
    if(front==rear)
    {
        printf("Queue is empty\n");
    }
    else
    {
        int i;
        printf("Deleted item is: %d",queue[front]);
        for(i=0;i<rear;i++)
            queue[i]=queue[i+1];
    }
}

void traverse()
{
    if(front==rear)
    {
        printf("Queue is empty\n");
    }
    else
    {
        printf("Queue elements are: ");
        int i;
        for(i=front;i<rear;i++)
        {
            printf("%d ",queue[i]);
        }
        rear--;
    }
}
