/*Exercise-5
Write a function that returns index of first matched element from given array.
if element is not found it returns -1. e.g. if array[] = {1, 2, 5, 9, 11} and search value entered is "5" than it returns 2.
if search value entered is "7" which is not present inside array, so function returns -1.
*/


#include<stdio.h>
#define SIZE 100

int main()
{
    int arr[SIZE],i,element,flag=0;
    printf("Enter 10 elements of an array: ");
    for(i=0;i<10;i++)
    {
        scanf("%d",&arr[i]);
    }

    printf("\nGiven array is: ");
    for(i=0;i<10;i++)
    {
        printf("%d ",arr[i]);
    }

    printf("\nEnter an element want to find: ");
    scanf("%d",&element);
    for(i=0;i<10;i++)
    {
        if(arr[i] == element)
        {
            flag = 1;
            break;
        }
    }
    if(flag == 1)
        return i;
    else
        return -1;
}
