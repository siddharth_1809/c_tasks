Rem Batch script which copies folder_1, folder-2, folder-3 into out_fold and zip out_fold with timestamp as zipped folder name
@echo off

SET location=C:\Users\Siddharth\Desktop\Test

Rem This Command removes fractional part of ms
SET time=%TIME:~,5%

Rem This command takes a date and replaces '/' with '-' and also takes a time and replaces ':' with '-'
SET STAMP=%DATE:/=-%_%time::=-%

Rem This command adds name with timestamp to the folder which is going to zipped
SET STAMP=out_%STAMP%


echo Copying Folder-1
copy  %location%\folder_1 %location%\out_fold 

echo Copying Folder-1
copy  %location%\folder_2 %location%\out_fold 

echo Copying Folder-1
copy  %location%\folder_3 %location%\out_fold 

echo Zipping......
zip -r %STAMP% out_fold
echo Zipped successfully