#Shell script which copies folder_1, folder-2, folder-3 into out_fold and zip out_fold with timestamp as zipped folder name
#! /bin/bash

f1=folder_1
f2=folder_2
f3=folder_3
dest=out_fold

echo "Copying folder-1"
cp -R $f1 $dest

echo "Copying folder-2"
cp -R $f2 $dest

echo "Copying folder-3"
cp -R $f3 $dest

echo "Zipping"
today=`date '+%d-%m-%Y_%H-%M-%S'`
filename="out_fold_$today"

zip -r $filename $dest

echo "zipping successful"