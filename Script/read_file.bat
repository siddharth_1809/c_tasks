@Echo off
Rem This script read the content of given file from filename given by user if it exist


SET location=C:\Users\Siddharth\Desktop\Test
cd %location%

echo List of files in %location% are :
 
dir *.txt

SET /p file=Enter a Filename:

If exist %file% (echo Reading content from %file% 
				more %file%) else (echo %file% doesn't exist)

set /p str=Enter a string want to find:

Rem find command matches the string in file and if matches then /n returns line number and text while /i ignores the case
find /n /i "%str%" %file%			
	if %errorlevel%==1 (echo search string not found) else (echo search found)
	
	