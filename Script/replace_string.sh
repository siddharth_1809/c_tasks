#! /bin/bash
#This script reads the data of specified file and ask user to replace a word with another word entered by user

file=text.txt
echo "Reading from file"
echo
cat $file

echo
echo -e "Enter a word to find : \c"
read find
echo -e "Enter a word to replace : \c"
read replace
echo

echo "Replacing $find with $replace"

sed -i "s/$find/$replace/g" $file	#s specifies substitution operation /g is used for replace all the occurrences similary we can use /1 and /2
echo "Updated file : "
cat $file