@echo off

SET location=C:\Users\Siddharth\Desktop\Test
cd %location%

SET file=test.txt
SET temp=new.txt
SET /p search=Search word : 
SET /p replace=Replace word : 

echo Content of file before replace
more %file%


(for /f "delims=" %%A in (%file%) do ( 

	set "line=%%A"
	Rem expands variable at execution time rather than at parse time
	setlocal enabledelayedexpansion	
	
	Rem !name! is syntax used to print variable value when you use enabledelayedexpansion 
	set "line=!line:%search%=%replace%!"
	echo !line!
	endlocal)
)> %temp%

echo Updated file
more %temp%
