#! /bin/bash

IFS=$'\n'      
array=($(sudo iwlist wlo1 scan | grep ESSID | sed -r 's/^.{26}//' | sed -e 's/^"//' -e 's/"$//' ))


for (( i=0; i<${#array[@]}; i++ ))
do
    echo "$i: ${array[$i]}"
done

echo -e "Enter a SSID Number : \c"
read SSID

echo -e "Enter Password for ${array[$SSID]} : \c"
read password

nmcli d wifi connect "${array[$SSID]}" password "$password" ifname wlo1

if [[ "$?" != 0 ]]; 
then
	echo "Command Error"
else
	echo "Command Success"
	ping 8.8.8.8 -c 1
	echo "WiFi test successful"
fi
