#changing IP address of wifi from static to dynamic and vice-versa
import os

def dynamic_to_static():
	address=input("Enter an IP address : ")
	netmask=input("Enter netmask : ")
	gateway=input("Enter gateway : ")
	
	with open('interfaces','w') as tmp:		
		tmp.write('auto lo\niface lo inet loopback\n\n')
		tmp.write('auto wlo1\niface wlo1 inet static\n\t')
		tmp.write('address {}\n\tnetmask {}\n\tgateway {}\n\t'.format(address,netmask,gateway))
		tmp.write('dns-nameservers 8.8.8.8')
	
	os.system('mv interfaces /etc/network/interfaces')
	os.system('sudo ip a flush wlo1')
	os.system('sudo systemctl restart networking.service')
	print('Operation Successful')
	print('Hit ifconfig command to check your IP address')

def static_to_dynamic():
	with open('interfaces','w') as tmp:		
		tmp.write('auto lo\niface lo inet loopback\n\n')
		tmp.write('auto wlo1\niface wlo1 inet dhcp')
	os.system('mv interfaces /etc/network/interfaces')
	os.system('sudo ip a flush wlo1')
	os.system('sudo systemctl restart networking.service')
	print('Operation Successful')
	print('Hit ifconfig command to check your IP address')

choice=int(input("1. Dynamic to static :\n2. Static to dynamic :\nEnter choice : ")) 
if(choice==1):
	dynamic_to_static()
elif(choice==2):
	static_to_dynamic()
else:
	print("Enter a valid choice")
