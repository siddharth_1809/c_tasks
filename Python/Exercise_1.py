'''
Write a C program that display given number in string format.
e.g. 10 -> ten 100 -> hundred 5270 -> five thousand two hundred seventy
'''
ones = ("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine")
elevens = ("Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen")
tens = ("", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety")

num = int(input("Enter 4 digit number : "))     #taking user input
temp = num
arr = []
while(temp > 0):
    arr.append(temp%10)
    temp //= 10                 #use floor divison
 
i = len(arr)

if i==4:
    print(ones[arr[3]],"Thousands", end=" ")

if (i>=3 and arr[2] != 0):
    print(ones[arr[2]],"Hundred", end=" ")

if i>=2:
    if arr[1] == 0:
        print(ones[arr[0]], end=" ")
    elif arr[1] == 1:
        print(elevens[arr[0]])
    else:
        print(tens[arr[1]], ones[arr[0]], end=" ")

if (i == 1 and num!=0):
    print(ones[arr[0]], end=" ")

if num==0:
    print("Zero")
    
