'''
Write a function that expands given short norm to expanded string.
e.g. if passed string is "a-g" output should be "abcdefg" if passed string is "b-h4-8" output should be "bcdefgh45678"
if passed string is "-c-e-" output should be "cde"
'''

input_str = input("Enter a string : ")

for i in input_str:
    final_str = input_str.replace('-','')

print("After removing character '-' final string is:",final_str)
alpha = ""
num = ""

for i in range(len(final_str)):
    if ((final_str[i] >='A' and final_str[i] <='Z') or (final_str[i] >='a' and final_str[i] <='z')):
        alpha += final_str[i]
    else:
        num += final_str[i]
    
for i in range(ord(alpha[0]),(ord(alpha[len(alpha)-1]))+1):     #ord() gives ASCII value
    print(chr(i),end= " ")                                      #chr() convert int to char

for j in range(ord(num[0]),(ord(num[len(num)-1]))+1):     #ord() gives ASCII value
    print(chr(j),end= " ")



    
