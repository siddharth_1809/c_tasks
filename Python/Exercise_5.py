'''
Write a function that returns index of first matched element from given array.
if element is not found it returns -1.
e.g. if array[] = {1, 2, 5, 9, 11} and search value entered is "5" than it returns 2.
if search value entered is "7" which is not present inside array, so function returns -1.
'''

def find_element(num_list):
    search = int(input("Enter an element you want to find : "))
    for i in range(len(num_list)):
        if num_list[i] == search:
            flag = 1
            break
        else:
            flag = 0
    if flag==1:
        return i
    else:
        return -1
        
count = int(input("Number of elements you want in list : "))
num_list = []
for i in range(count):
    element = int(input("Enter element : "))
    num_list.append(element)

print("List is :",num_list)
print(find_element(num_list))


