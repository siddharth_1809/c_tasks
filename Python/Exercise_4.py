'''
Write a C function to invert the bits in given integer variable.
The inversion of bits should start from given position
and should be done upto given counts.
'''

def invert(num,position,count):
    start = position
    for i in range(count):
        num = num ^ (1<<position)
        position += 1
    print("After inverting from position",start,"upto count",count,"result is",bin(num))

num = int(input("Enter a Number: "))
position = int(input("Enter a Position : "))
count = int(input("Enter a Count : "))

print("Input is",bin(num))
invert(num,position,count)



