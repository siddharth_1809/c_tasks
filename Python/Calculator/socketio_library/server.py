from flask import Flask
from flask_socketio import SocketIO

app = Flask(__name__)
app.debug = True
socketio = SocketIO(app)

@socketio.on('msg')
def on_message(message):
    print(message)
    temp = message

    if temp['operation'] == '+':
        socketio.emit('response',temp["num1"] + temp["num2"])
    elif temp['operation'] == '-':
        socketio.emit('response',temp["num1"] - temp["num2"])
    elif temp['operation'] == '*':
        socketio.emit('response',temp["num1"] * temp["num2"])
    elif temp['operation'] == '/':
        socketio.emit('response',temp["num1"] / temp["num2"])
    elif temp['operation'] == 'E' or 'e':
        socketio.emit('response','Client choose exit')
    else:
        socketio.emit('response',"Enter a valid operation")

@socketio.on('exit')
def on_exit(message):
    print(message)

socketio.run(app,host='0.0.0.0',port=8000)
