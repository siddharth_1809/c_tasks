from socketIO_client import SocketIO
def on_response(response):
    print("Result is",response)

def disconnect():
    print('disconnected from server')

socketIO = SocketIO('127.0.0.1', 8000)
if socketIO is not None:
    socketIO.on('response', on_response)

    num1 = int(input("Enter Number-1 : "))
    num2 = int(input("Enter Number-2 : "))
    print("Choose from \n Addition '+'\tSubtraction '-'\tMultiplication '*'\tDivision '/'\tQuit 'E' or 'e' :\n")
    choice = input("Enter operation : ")

    numerical = {'num1': num1, 'num2': num2, 'operation': choice}

    if numerical['operation'] == ('E' or 'e'):
        socketIO.emit('exit',"Client Exit")
        disconnect()

    socketIO.emit('msg', numerical)

    socketIO.wait()

