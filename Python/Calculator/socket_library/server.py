import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    #also can write simply socket.socket()
print("Socket Created")

sock.bind(('localhost',9999))
print("Bind Connection Successfully")

sock.listen(5)
print("Waiting for connection")

client, addr = sock.accept()        #returns client socket and address
print("Coonection with",addr)

client.send("TCP Calculator".encode())  #also can use bytes("TCP Calculator",'utf-8')
#for simplicity we use .encode() because on client side we use .decode()

while True:
    num1 = client.recv(1024).decode()
    num1 = int(num1)
    print("Num-1 : ",num1)

    num2 = client.recv(1024).decode()
    num2 = int(num2)
    print("Num-2 : ",num2)

    choice = client.recv(1024).decode()
    choice = int(choice)
    print("Choice is : ",choice)

    if choice == 1:
        result = num1 + num2
        client.send(str(result).encode())

    elif choice == 2:
        result = num1 - num2
        client.send(str(result).encode())

    elif choice == 3:
        result = num1 * num2
        client.send(str(result).encode())

    elif choice == 4:
        result = num1 // num2
        client.send(str(result).encode())

    elif choice == 5:
        break

    else:
        client.send("Choose a valid option".encode())

client.close()
