from flask import Flask, request
from flask_socketio import SocketIO
app = Flask(__name__)
app.debug = True
socketio = SocketIO(app)

@socketio.on('msg')
def on_message(message):
    print(message)
    temp = message

    if temp['operation'] == '+':
        socketio.emit('response',temp["num1"] + temp["num2"],room=request.sid)
    elif temp['operation'] == '-':
        socketio.emit('response',temp["num1"] - temp["num2"],room=request.sid)
    elif temp['operation'] == '*':
        socketio.emit('response',temp["num1"] * temp["num2"],room=request.sid)
    elif temp['operation'] == '/':
        socketio.emit('response',temp["num1"] / temp["num2"],room=request.sid)
    else:
        socketio.emit('response',"Enter a valid operation",room=request.sid)

@socketio.on('exit')
def on_exit(message):
    print(message)

"""
@socketio.on('client-2')
def data(message):
    print("Message from client-2 : ",message)
    fullname = message['fname'] + ' ' + message['lname']
    email = message['fname'] + '.' + message['lname'] + '@gmail.com'
    result = {'fullname':fullname, 'email':email}
    socketio.emit('response-1',result)
"""

socketio.run(app,host='0.0.0.0',port=8000)