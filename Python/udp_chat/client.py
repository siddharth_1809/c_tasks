#Server client basic chat
import socket

client_socket=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

client_socket.sendto("Hello from client !".encode('utf-8'), ("127.0.0.1", 8080))
data, address = client_socket.recvfrom(1024)
print(data.decode('utf-8'))

while True:
    send_data = input("Type message to send : ")
    client_socket.sendto(send_data.encode('utf-8'),("127.0.0.1",8080))
    received_data, address = client_socket.recvfrom(1024)
    print("Server : ",received_data.decode('utf-8'))
