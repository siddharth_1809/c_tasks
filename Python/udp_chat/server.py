#Server client basic chat 
import socket

socket=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
address="127.0.0.1"
port=8080
socket.bind((address,port))

data, address = socket.recvfrom(1024)
print(data.decode('utf-8'))
socket.sendto("Hello From Server !".encode('utf-8'), address)

while True:
    received_data, address = socket.recvfrom(1024)
    print("Client : ",received_data.decode('utf-8'))
    send_data =input("Type message to send : ")
    socket.sendto(send_data.encode('utf-8'),address)
