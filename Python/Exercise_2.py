'''
Write a C program to find 1's count in binary value of given integer number.
'''

num = int(input("Enter a number : "))   #explicit conversion internally it takes input as string 
temp = num
count = 0
while(num != 0):
    if (num & 1 == 1):
        count = count + 1
    num = num>>1

print("Number of 1's in",bin(temp),"are",count)
