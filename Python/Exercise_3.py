'''
Write a C program to reverse the given number in most efficient way.
'''

num = int(input("Enter a Number : "))   #explicit conversion
result = 0
while(num > 0):
    rem = num%10
    result = result*10 + rem
    num //= 10                  #use floor division

print("After reversing",result)
       
