'''
Make an example of fifo
'''

count = int(input("Enter size of a queue : "))
num_list = []

def insert():
    if (len(num_list) == count):
        print("Queue is full\n")
    else:
        element = int(input("Enter element : "))
        num_list.append(element)
        print("Queue is : ",num_list)


def delete():
    i = 0
    if (len(num_list) != 0):
        del num_list[i]
        i+=1
        print("Queue is : ",num_list)
    else:
        print("Queue is empty : ")


def traverse():
    if (len(num_list) == 0):
        print("Queue is empty")
    else:
        print("Queue is :",num_list)
    

while(1):
    choice = int(input("Enter a choice\n1-insert :\n2-delete :\n3-traverse :\n"))
    if choice == 1:
        insert()
        
    elif choice == 2:
        delete()
        
    elif choice == 3:
        traverse()
        
    else:
        print("Enter a valid choice\n")
