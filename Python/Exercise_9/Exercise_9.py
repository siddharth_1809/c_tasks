'''
Make function to compare two strings (returns 0 on success and -1 on failure)
Define above function in another c file instead of main source file.
Make a Makefile to compile these c files.
'''

from function import str_cmp    #function is .py file and it contains definition of str_cmp

str1 = input("Enter string-1 : ")
str2 = input("Enter string-2 : ")
print(str_cmp(str1,str2))
